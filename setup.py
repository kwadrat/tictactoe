# -*- coding: UTF-8 -*-
from setuptools import setup


setup(
    name = "Tic Tac Toe",
    version = "0.1",
    packages = ['tictactoe'],

    # metadata for upload to PyPI
    author = "Maciej Wisniowski, Piotr Kasprzyk",
    author_email = "maciej.wisniowski@natcam.pl",
    description = "Tic Tac Toe",
    license = "wtfpl",
    keywords = "tic tac toe kółko krzyżyk",
)
