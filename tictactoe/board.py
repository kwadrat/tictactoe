# -*- coding: UTF-8 -*-
from itertools import chain


class Board:
    X = 1
    O = 2

    def __init__(self):
        self.clear()

    def conv_2_one_dim(self, x, y):
        """ Convert x, y coordinates to position in one dimensional table
        """
        return y * 3 + x

    def clear(self):
        """ Use one dimensional table with length of 9 to store information about 3x3 matrix
        """
        self._board = [None] * 9

    def do_move(self, x, y, player):
        """ Put X or O at the board - if it is possible to do it
        """
        _x = self.conv_2_one_dim(x, y)
        status = self.is_valid_move(_x)
        if status:
            self._board[_x] = player
        return status

    def is_valid_move(self, x):
        """ It is not possible to override existing X or O
        """
        try:
            return self._board[x] is None
        except IndexError as e:
            return False

    def _same_three(self, start, step):
        """ Check if three elements of the table are same. Elements are chosen by using start and step:
            1: start, 2: start + step, 3: start + 2 * step
        """
        end = start + 3 * step
        uniq = set(self._board[start:end:step])
        return len(uniq) == 1 and uniq.pop() is not None

    def is_finished(self):
        return any([
            # rows
            self._same_three(0, 1),
            self._same_three(3, 1),
            self._same_three(6, 1),

            # cols
            self._same_three(0, 3),
            self._same_three(1, 3),
            self._same_three(2, 3),

            # diagonals
            self._same_three(0, 4),
            self._same_three(2, 2)]
        )
