# -*- coding: UTF-8 -*-
from board import Board
from renderer import Renderer


class TestBoard:

    def setUp(self):
        self.board = Board()

    def test_conv(self):
        assert self.board.conv_2_one_dim(0, 0) == 0
        assert self.board.conv_2_one_dim(1, 0) == 1
        assert self.board.conv_2_one_dim(0, 1) == 3
        assert self.board.conv_2_one_dim(1, 2) == 7

    def test_is_valid_move(self):
        assert self.board.is_valid_move(1)
        assert not self.board.is_valid_move(10)
        self.board._board[4] = Board.X
        assert not self.board.is_valid_move(4)

    def test_do_move(self):
        self.board.do_move(1, 1, Board.X)
        assert self.board._board[4] == Board.X
        self.board.do_move(1, 1, Board.O)
        assert self.board._board[4] == Board.X
        self.board.do_move(0, 0, Board.O)
        assert self.board._board[0] == Board.O

    def test_same_three(self):
        X = Board.X
        O = Board.O
        self.board._board = [X, X, X, O, O, O, O, O, O]
        assert self.board._same_three(0, 1)

        self.board._board = [X, O, X, O, O, O, O, O, O]
        assert not self.board._same_three(0, 1)

        self.board._board = [X, O, O, X, O, O, X, O, O]
        assert self.board._same_three(0, 3)

        self.board._board = [O, O, O, X, O, O, X, O, O]
        assert self.board._same_three(0, 1)

    def test_is_finished(self):
        X = Board.X
        O = Board.O
        _ = None
        assert not self.board.is_finished()
        self.board._board = [X, X, O,
                             X, X, O,
                             X, _, _]
        assert self.board.is_finished()

        self.board._board = [X, X, O,
                             X, O, O,
                             O, O, X]
        assert self.board.is_finished()

        self.board._board = [X, _, _,
                             O, X, O,
                             X, O, X]
        assert self.board.is_finished()

        self.board._board = [O, _, _,
                             X, O, X,
                             O, X, O]
        assert self.board.is_finished()

        self.board._board = [O, O, X,
                             O, O, X,
                             O, _, _]
        assert self.board.is_finished()

        self.board._board = [O, O, X,
                             O, O, X,
                             _, O, _]
        assert self.board.is_finished()

        self.board._board = [O, X, O,
                             O, X, O,
                             _, _, O]
        assert self.board.is_finished()

        self.board._board = [_, _, O,
                             X, O, X,
                             O, X, O]
        assert self.board.is_finished()

        self.board._board = [O, O, O,
                             O, O, _,
                             X, X, _]
        assert self.board.is_finished()

        self.board._board = [O, O, _,
                             O, O, O,
                             X, X, _]
        assert self.board.is_finished()

        self.board._board = [O, O, _,
                             X, X, _,
                             O, O, O]
        assert self.board.is_finished()


class TestRenderer:
    def setUp(self):
        self.board = Board()
        self.renderer = Renderer()

    def test_map_player(self):
        assert self.renderer._map_player(Board.X) == 'X'
        assert self.renderer._map_player(Board.O) == 'O'
        assert self.renderer._map_player(None) == '.'

    def test_prompt_player(self):
        assert self.renderer.prompt_player(Board.X) == 'Gracz: X'
        assert self.renderer.prompt_player(Board.O) == 'Gracz: O'

    def test_render_empty_table(self):
        assert self.renderer.render_board(self.board) == '...\n' * 3

    def test_render_table(self):
        self.board._board[1] = Board.X
        self.board._board[8] = Board.O

        outboard =  '.X.\n'
        outboard += '...\n'
        outboard += '..O\n'
        assert self.renderer.render_board(self.board) == outboard

