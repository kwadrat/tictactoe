# -*- coding: UTF-8 -*-
from __future__ import print_function
from board import Board

class Renderer:
    def _map_player(self, player):
        return {None: '.', Board.X: 'X', Board.O: 'O'}[player]

    def render_board(self, board):
        fragments = []
        for pos, x in enumerate(board._board):
            icon = self._map_player(x)
            fragments.append(icon)
            if (pos + 1) % 3 == 0:
                fragments.append('\n')
        out = ''.join(fragments)
        print(out)
        return out

    def prompt_player(self, player):
        out = "Gracz: %s" % self._map_player(player)
        print(out)
        return out

    def get_coords(self, player):
        self.prompt_player(player)
        try:
            x = int(raw_input('Podaj numer kolumny (0-2): '))
            y = int(raw_input('Podaj numer wiersza (0-2): '))
        except (TypeError, ValueError) as e:
            return None
        return x, y

    def render_game_finished(self, board):
        print("Finished!")
        self.render_board(board)
