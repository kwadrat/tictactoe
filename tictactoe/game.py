# -*- coding: UTF-8 -*-
from __future__ import print_function
from random import choice
from board import Board
from renderer import Renderer


class Game:
    def run(self):
        board = Board()
        renderer = Renderer()
        # choose random player
        player = choice([Board.X, Board.O])

        while not board.is_finished():
            # every turn switch player
            player = Board.X if player == Board.O else Board.O
            renderer.render_board(board)

            res = False
            # try hard to get only valid coords
            while not res:
                x_y = renderer.get_coords(player)
                if x_y:
                    res = board.do_move(x_y[0], x_y[1], player)

        renderer.render_game_finished(board)


if __name__ == '__main__':
    game = Game()
    game.run()
