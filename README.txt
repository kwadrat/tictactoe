Instalacja i uruchamianie:

1. Utworzyć virtualenv, np. przy pomocy virtualenvwrappera:
   mkvirtualenv tictactoe
   workon tictactoe

3. Zainstalować nose
   pip install nose

4. Odpalić testy

  nostetests
lub
  nosetests -s   # bez przechwytywania stdout


5. Uruchomienie gry: python game.py

Diagram sekwencji z folderu doc/ został utworzony w narzędziu UMLet.
